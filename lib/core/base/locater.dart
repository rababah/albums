import 'package:albums/core/feature/albums_list/album_service.dart';
import 'package:albums/core/feature/albums_list/album_view_model.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  /// Services
  locator.registerLazySingleton(() => AlbumService());

  /// View Model
  locator.registerLazySingleton(() => AlbumsViewModel());




}