
class BaseUtils {

  static generateContactAdminMsg([err = null]) {
    //TODO: Add translation
    String localMsg = 'Something wrong happened, please contact the admin';
    if (err != null) {
      localMsg = localMsg + '\n \n' + err.toString();
    }
    return localMsg;
  }


}