import 'dart:async';

import 'package:albums/core/base/view_state.dart';
import 'package:flutter/material.dart';

class BaseViewModel extends ChangeNotifier {

  ViewState _state = ViewState.Idle;

  ViewState get state => _state;

  String error = "";
  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }


}
