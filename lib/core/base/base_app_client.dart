
import 'dart:convert';
import 'dart:io' show Platform;
import 'package:albums/core/config/config.dart';
import 'package:http/http.dart' as http;


class BaseAppClient {
  static get(String endPoint,
      {required Map<String, dynamic> body,
        required Function(dynamic response, int statusCode) onSuccess,
        required Function(String error, int statusCode) onFailure, Map<String, String> ? queryParameters,

      }) async {
    String url =  endPoint;

   if(queryParameters!=null){
     String queryString = Uri(queryParameters: queryParameters).query;
     url = url+queryString;
   }

    try {

      print("URL : $url");
      print("Body : ${json.encode(body)}");
        final response = await http.get(Uri.parse(url),
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            });
        final int statusCode = response.statusCode;
        if (statusCode < 200 || statusCode >= 400) {
          onFailure(getError(response), statusCode);
        } else {
          var parsed = json.decode(response.body.toString());
          onSuccess(parsed, statusCode);
        }

    } catch (e) {
      print(e);
      onFailure(e.toString(), -1);
    }
  }


  static String getError(parsed) {
    String error = parsed.message ;
    return error;
  }
}
