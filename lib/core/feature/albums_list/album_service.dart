
import 'package:albums/core/base/base_service.dart';
import 'package:albums/core/config/config.dart';
import 'package:albums/core/feature/albums_list/models/album_response_model.dart';

import '../../base/base_app_client.dart';

class AlbumService extends BaseService {
   AlbumResponseModel? albumResponseModel;
  Future geAlbums() async {
    hasError = false;
    await BaseAppClient.get(
      GET_ALL_Albums,
      onSuccess: (dynamic response, int statusCode) {
       albumResponseModel = AlbumResponseModel.fromJson(response);
      },
      onFailure: (String error, int statusCode) {
        hasError = true;
        super.error = error;
      },
      body: {},
    );
  }

}