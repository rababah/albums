
import 'package:albums/core/base/view_state.dart';
import 'package:albums/core/feature/albums_list/album_service.dart';
import 'package:albums/core/feature/albums_list/models/album_response_model.dart';

import '../../base/base_view_model.dart';
import '../../base/locater.dart';

class AlbumsViewModel extends BaseViewModel {
  AlbumService _albumService = locator<AlbumService>();
  List<Album> filterData = [];

  Future geAlbums() async {
    setState(ViewState.Busy);
    await _albumService.geAlbums();
    if (_albumService.hasError) {
      error = _albumService.error;
      setState(ViewState.Error);
    } else {
      filterData = _albumService.albumResponseModel!.results!.albummatches!.album!;
      setState(ViewState.Idle);
    }


  }

  List<String> getImagesWithGoodFormate(List<LocalImage>? images) {
    List<String> imgList = [];
    if (images != null) {
      images.forEach((element) {
        if (element.size == "small") imgList.add(element.text!);
      });
    }
    return imgList;
  }

}