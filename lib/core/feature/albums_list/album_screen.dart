import 'package:albums/core/feature/albums_list/album_view_model.dart';
import 'package:albums/core/feature/albums_list/models/album_response_model.dart';
import 'package:flutter/material.dart';

import '../../base/locater.dart';
import '../../base/view_state.dart';
import 'package:provider/provider.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_slider.dart';

class AlbumScreen extends StatefulWidget {
  AlbumsViewModel model = locator<AlbumsViewModel>();

  AlbumScreen({Key? key}) : super(key: key);

  @override
  State<AlbumScreen> createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Future<void> didChangeDependencies() async {
    await widget.model.geAlbums();

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardB = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
    );
    return Scaffold(
        appBar: AppBar(
          title: Text("Albums"),
        ),
        body: ChangeNotifierProvider<AlbumsViewModel>.value(
          value: widget.model,
          child: Consumer<AlbumsViewModel>(
            builder: (context, _, __) {
              return buildBaseViewWidget();
            },
          ),
        ));
  }

  buildBaseViewWidget() {
    switch (widget.model.state) {
      case ViewState.Idle:
        return Container(
          child: widget.model.filterData.isEmpty
              ? const Center(
                  child: Text(
                    "No Orders",
                  ),
                )
              : Center(
                  child: FractionallySizedBox(
                    widthFactor: 0.9,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: widget.model.filterData.length,
                        itemBuilder: (BuildContext ctxt, int index) {
                          final GlobalKey<ExpansionTileCardState> cardA =
                              new GlobalKey();

                          return ExpansionTileCard(
                            key: cardA,
                            leading: CircleAvatar(
                                child: Text(
                                    '${widget.model.filterData[index].name!.substring(0, 1)}')),
                            title:
                                Text('${widget.model.filterData[index].name!}'),
                            subtitle: Text(
                                '${widget.model.filterData[index].artist!}'),
                            children: <Widget>[
                              Divider(
                                thickness: 1.0,
                                height: 1.0,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0,
                                    vertical: 8.0,
                                  ),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          CarouselSlider(
                                            options: CarouselOptions(
                                                enableInfiniteScroll: false),
                                            items: widget.model.getImagesWithGoodFormate(
                                                    widget
                                                        .model
                                                        .filterData[index]
                                                        .image!)
                                                .map((item) => Container(
                                                      child: Center(
                                                          child: Image.network(
                                                              item,
                                                              fit: BoxFit.cover,
                                                              width: 1000)),
                                                    ))
                                                .toList(),
                                          ),
                                          Positioned(
                                            left: 150,
                                            top: 70,
                                            child: Container(
                                              child: InkWell(
                                                  child: Icon(Icons.play_arrow),
                                                  onTap: () => launch(
                                                      '${widget.model.filterData[index].url!}')),
                                              color: Colors.white,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        }),
                  ),
                ),
        );
        break;
      case ViewState.Busy:
        return Center(child: CircularProgressIndicator());
        break;
      case ViewState.Error:
        return Text(
          widget.model.error,
        );
        break;
    }
  }


}
